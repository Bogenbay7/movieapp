
import Foundation

struct Detail: Decodable {
    var id: Int?
    let original_title: String
    let overview: String
    let release_date: String
    let vote_average: Double
    let poster_path: String
}


import Foundation

struct MovieEntity: Decodable {
    let results:[Movie]
    
    struct Movie: Decodable {
        let id: Int
        let poster: String?
        let title: String?
        let releaseData: String?
        let rating: Double?
        
        enum CodingKeys: String, CodingKey {
            case id = "id"
            case poster = "poster_path"
            case title = "title"
            case releaseData = "release_date"
            case rating = "vote_average"
        }
        
    }
}

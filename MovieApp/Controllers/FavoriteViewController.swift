//
//  FavoriteViewController.swift
//  MovieApp
//
//  Created by Nurba on 21.05.2021.
//

import UIKit

class FavoriteViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var movies: [MovieEntity.Movie] = []{
        didSet(oldValueMovies){
            if movies.count != oldValueMovies.count{
                tableView.reloadData()
            }
        }
    }
    private let coreDataManager = CoreDataManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.register(UINib(nibName: MainCell.identifier, bundle: Bundle(for: MainCell.self)), forCellReuseIdentifier: MainCell.identifier)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        movies = coreDataManager.allMovie()
    }


}
extension FavoriteViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MainCell.identifier, for: indexPath) as! MainCell
        cell.movie = movies[indexPath.row]
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    
    
}

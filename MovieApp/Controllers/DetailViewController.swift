
import UIKit
import Alamofire
import Kingfisher


class DetailViewController: UIViewController {
    @IBOutlet weak var favorite: UIButton!
    
    var movieId: Int!
    var decoder: JSONDecoder = JSONDecoder()
    public var jsonData: Detail? {
        didSet {
            if jsonData != nil {
                let posterURL = URL(string:"https://image.tmdb.org/t/p/w500" + (jsonData?.poster_path ?? ""))
                posterDImage.kf.setImage(with: posterURL)
                ratingD.text = "\(jsonData?.vote_average ?? 1.0)"
                TitleDLabel.text = jsonData?.original_title
                descriptionD.text = jsonData?.overview
                releaseD.text = jsonData?.release_date
        }
    }
    }
   
    
    @IBOutlet weak var posterDImage: UIImageView!
    @IBOutlet weak var ratingD: UILabel!
    @IBOutlet weak var viewD: UIView!
    @IBOutlet weak var TitleDLabel: UILabel!
    @IBOutlet weak var releaseD: UILabel!
    @IBOutlet weak var descriptionD: UILabel!
    private let coreDataManager = CoreDataManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewD.layer.cornerRadius = 20
        view.layer.masksToBounds = true
        getDetail()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let context = coreDataManager.persistentContainer.viewContext
        
        if let movieId = movieId{
            if let _ = MoviesEntity.findMovie(with: movieId, context: context){
                favorite.setImage(UIImage(named: "starFill"), for: .normal)
            }else{
                favorite.setImage(UIImage(named: "star"), for: .normal)
            }

        }
    }
    @IBAction func favoritePressed(_ sender: Any) {
        let context = coreDataManager.persistentContainer.viewContext
        
        if let movieId = movieId{
            if let _ = MoviesEntity.findMovie(with: movieId, context: context){
                favorite.setImage(UIImage(named: "star"), for: .normal)
                coreDataManager.delete(with: movieId)
            }else{
                if var jsonData = jsonData{
                    favorite.setImage(UIImage(named: "starFill"), for: .normal)
                    jsonData.id = movieId
                    coreDataManager.addMovie(jsonData)
                }
               
            }

        }
    }
    
    func getDetail() {
        let movID = movieId
        
        let detailURL: String = "https://api.themoviedb.org/3/movie/\(String(describing: movID!))?api_key=fd02cda11686345033c923064ac644da&language=en-US"

        AF.request(detailURL, method: .get).responseJSON { (response) in
            switch response.result{
            case .success(_):
                guard let data = response.data else { return }
                guard let answer = try? self.decoder.decode(Detail.self, from: data) else{ return }
              
                self.jsonData = answer
                print(self.jsonData!)
                
            case .failure(let err):
                print(err.errorDescription ?? "")
            }
        }
      
    }
    
 



}




import UIKit
import Alamofire

class ViewController: UIViewController {
    
    private let url: String = "https://api.themoviedb.org/3/trending/movie/week?api_key=fd02cda11686345033c923064ac644da"
    
   
    private var movies: [MovieEntity.Movie] = [MovieEntity.Movie](){
        didSet {
            tableView.reloadData()
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: MainCell.identifier, bundle: Bundle(for: MainCell.self)), forCellReuseIdentifier: MainCell.identifier)
        getMovies()
        // Do any additional setup after loading the view.
    }

    
    func getMovies() {
        AF.request(url, method: .get, parameters: [:]).responseJSON { (data) in
            switch data.result {
            case .success:
                if let  result = data.data{
                    do{
                        let movieJSON = try JSONDecoder().decode(MovieEntity.self, from: result)
                        self.movies = movieJSON.results
                    }catch{
                        print(error)
                    }
                }
            case .failure:
                print("error")
            }
        }
    }

}

extension ViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MainCell.identifier, for: indexPath) as! MainCell
        cell.movie = movies[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        if let vc = storyboard.instantiateViewController(withIdentifier: "detail") as? DetailViewController{
            vc.movieId = movies[indexPath.row].id
            print(movies[indexPath.row].id)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
}
